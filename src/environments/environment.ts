// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAGX3lDIiAwFxR9Js77NRLckIZcz4N7B8U",
    authDomain: "fbreg-a51c7.firebaseapp.com",
    databaseURL: "https://fbreg-a51c7.firebaseio.com",
    projectId: "fbreg-a51c7",
    storageBucket: "fbreg-a51c7.appspot.com",
    messagingSenderId: "775797295993"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
