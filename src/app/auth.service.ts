import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  register(email:string, password:string) // אפשר לקרוא לפונקציה בשם אחר. אין קשר לפונקצייה סיינאפ השנייה בהרשמה
  {
    return this.fireBaseAuth
              .auth
              .createUserWithEmailAndPassword(email,password);
  }

  updateProfile(user, name:string){
    // השם שיגיע מהסוגריים למעלה ייכנס לתוך דיספלייניימ שזהו שם שמור
    user.updateProfile({displayName:name, photoURL:''}); // אפדייטפרופייל זו פונקציה של יוזר שמעדכנת את הפרופיל של יוזר
  }

  isAuth()
  {
    return this.fireBaseAuth.authState.pipe(map(auth => auth));
  }

  login(email:string, password:string)
  {
    return  this
            .fireBaseAuth
            .auth
            .signInWithEmailAndPassword(email, password);
  }

  logout()
  {
    return this
          .fireBaseAuth
          .auth
          .signOut();
  }

  addUser(user, name: string, email: string)
  {
    let uid = user.uid;
    let ref = this.db.database.ref('/');        
    ref.child('users').child('uid').child(uid).push({name:name, email: email}); 
  }

  user: Observable<firebase.User>;
  
  constructor(private fireBaseAuth: AngularFireAuth, 
              private db:AngularFireDatabase) 
  {
    this.user = fireBaseAuth.authState;
  }
  
}