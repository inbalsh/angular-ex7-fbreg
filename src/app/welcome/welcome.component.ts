import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from "@angular/router";

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

import {FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {


  name = '';
  user;
  isLogged = false;
  todos: any[];

  form = new FormGroup(
    {todo:new FormControl('')}
  )

  constructor(private db: AngularFireDatabase,
            public authService: AuthService,
            private route: ActivatedRoute,
            private router:Router, 
            public afAuth: AngularFireAuth ) 
  {
    this.afAuth.user.subscribe(userInfo=>{
      if(this.authService.isAuth())
      {
        this.user = userInfo;
      }});
   }

  ngOnInit() {
 //   this.getCurrentUser();

 let uid = this.authService.user.subscribe(
  val =>{
    if(!val) return;        
    let uid = val.uid;
    //let ref = this.db.database.ref();
    this.db.list('/user/uid/' + uid + '/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }
)

}
/*
    this.route.queryParams
      .subscribe(params => {
        console.log(params); 
        this.name = params.name;
        console.log(this.name); 
      });
 
  }

  getCurrentUser()
  {
    this.authService.isAuth().subscribe( auth =>
    {
      if(auth)
      {
        console.log('user logged');
        this.isLogged = true;
      }
      else
      {
        console.log('user isnt logged'); 
        this.isLogged = false;
      }
    })
  }
*/

addTodo(){
  let todo = {text:this.form.value.todo};
  this.authService.user.subscribe(
    val =>{
      let uid = val.uid;
      let ref = this.db.database.ref('/');        
      this.form.get('todo').setValue('');
      ref.child('users').child('uid').child(uid).child('todos').push(todo);
    }
  )        
}

  onLogout()
  {
   // console.log("logout");
    this.afAuth.auth.signOut();
    this.router.navigate(['/signup']);
  }

}